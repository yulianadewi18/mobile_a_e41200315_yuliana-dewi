class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'hilmy',
      message: 'Hello Hilmy',
      time: '12.00',
      profileUrl:
          'https://pm1.narvii.com/5990/c766af10f27d6e01a3bd7d07f03ff85905c6e149_hq.jpg'),
  ChartModel(
      name: 'riska',
      message: 'hello riska',
      time: '9 march',
      profileUrl:
          'https://otakotaku.com/asset/img/character/2015/09/arima-kousei.jpg'),
  ChartModel(
      name: 'vita',
      message: 'hello vita',
      time: '10 march',
      profileUrl:
          'https://www.kaorinusantara.or.id/wp-content/uploads/2015/04/Shigatsu-wa-Kimi-no-Uso-OP-Large-05.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://cdn-2.tstatic.net/style/foto/bank/images/your-lie-in-april_20170817_171249.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://4.bp.blogspot.com/-DQ7gp6D1kJU/WikkpBNi1PI/AAAAAAAAFYI/961T2vISxwwPV8wVUCxEFqaMKoc0Ed5-ACLcBGAs/s1600/surat.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'http://nuhafairusya.web.ugm.ac.id/wp-content/uploads/sites/8390/2015/07/kousei-crying-ep-13.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://pict.sindonews.net/dyn/600/pena/sindo-article/original/2021/10/03/TOKYOREVENGERS002.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://i.pinimg.com/736x/df/d6/42/dfd642f04177704090b8eaef26c55524.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://assets.pikiran-rakyat.com/crop/0x0:0x0/750x500/photo/2021/08/03/3318804131.png'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://pbs.twimg.com/media/EyXTamwVEAMd0vK.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://i.pinimg.com/236x/e2/3f/46/e23f464572f7cd1e3b5968d6782b0f4d.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://media-assets-ggwp.s3.ap-southeast-1.amazonaws.com/2021/07/biodata-kawaragi-senju-featured-640x360.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://www.greenscene.co.id/wp-content/uploads/2021/06/Tokyo-Revengers-10.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://cdn.mpotimes.id/uploads/images/2021/07/image_750x_60e984c3866f3.webp'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'http://assets.kompasiana.com/items/album/2021/08/01/screenshot-2021-08-01-11-04-15-49-61065c6b06310e170b638702.png'),
];
